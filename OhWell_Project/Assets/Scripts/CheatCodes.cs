﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatCodes : MonoBehaviour
{
    [SerializeField]
    private CheckpointManager _checkpointManager;

    [SerializeField]
    private Checkpoint _finalCheckpoint;
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Log("CHEAT!");
            _checkpointManager.AddNewCheckpoint(_finalCheckpoint);
        }
    }
}
