﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoisonedWater : MonoBehaviour
{
    [SerializeField]
    private float _waterSpeed = 2f;

    [SerializeField] 
    private Transform _stopPosition;

    private bool _isWaterTriggered;

    public float WaterSpeed 
    {
        get {return _waterSpeed;}
    }
    public bool IsWaterTriggered 
    {
        get {return _isWaterTriggered;}
        set { _isWaterTriggered = value;}
    }

    private void Update()
    {
        if (_isWaterTriggered && this.transform.position.y <= _stopPosition.position.y)
            MoveWaterUp();
    }

    private void MoveWaterUp()
    {
        this.transform.Translate(Vector2.up * _waterSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Enemy"))
        {
            // get possible health component
            Health health = other.gameObject.GetComponent<Health>();
            //check if health exists
            if (health != null)
            {
                health.Damage(100f);
            }                
        }
    }
}
