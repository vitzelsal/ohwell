﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class WaterTrigger : MonoBehaviour
{
    [SerializeField]
    private GameObject _poisonedWater; 

    [SerializeField, Header("Audio")]
    AudioMixer _mixer;

    [SerializeField]
    public AudioSource musLoop;
    [SerializeField]
    private float _intenseMusicVolume = -12.00f;
    [SerializeField]
    private float _maxWaterVolume = 0.0f;
    [SerializeField]
    private float _volumeOffset = 0.01f;

    private bool _playBackgroundMusic = false;

    private void Update()
    {
        if(musLoop.isPlaying) SetMusVolume();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            var poisonWater = _poisonedWater.GetComponent<PoisonedWater>();
            if (poisonWater != null)
            {
                StartMusLoop();
                poisonWater.IsWaterTriggered = true;
            }
        }
    }

    public void SetMusVolume()
	{
        float currentVolume;
        _mixer.GetFloat("musVolume", out currentVolume);  
        if(currentVolume == 0f)_mixer.SetFloat("musVolume", currentVolume+_volumeOffset);  
    }

    public void StartMusLoop()
    {
        if(!musLoop.isPlaying) musLoop.Play(); 
        _mixer.SetFloat("musVolume", _intenseMusicVolume);  
    }

}

