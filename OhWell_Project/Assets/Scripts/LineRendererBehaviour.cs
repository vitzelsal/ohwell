﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class LineRendererBehaviour : MonoBehaviour
{   
    [Header("Inputs")]
    [SerializeField] private float _speed = 5f;
    [SerializeField] private float _dragSpeed = 2.5f;

    [SerializeField] private float _maxLength = 15f;
    [SerializeField] private Transform _pivotObject = null;
    [SerializeField] private LayerMask _enemyCheckMask;

    private LineRenderer _lineRenderer;
    private CapsuleCollider2D _collider;
    public UnityEvent updateLengthEvent;


    [Header("TemporaryDebugger")]
    [SerializeField] public bool _incrementLengthValue = false;
    [SerializeField] private bool _decrementLengthValue = false;
    [SerializeField] private bool _isMoving = false;
    [SerializeField] private float _lineLength = 0f;
    [SerializeField] private float _enemyPositionOffset = -4f;
    [SerializeField] private bool _enemyFound = false;
    [SerializeField] private Vector3 _endPointLine;
    [SerializeField] private Vector2 _enemyPosition;


     // properties
    public bool DebugObject {get; private set;} = false;
    public float Speed {get; private set;} 
    public float MaxLength { get; private set; } 
    public float LineLength { get; private set; } 
    public bool IsMoving {get; private set;} 
    public bool EnemyFound {get; private set;} 

    
    private Player _player; 
    private CharacterController2D _playerController;
    private Rigidbody2D _playerRb;

    private float _initialGravityScale;
    
    private void Start()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        _collider = GetComponent<CapsuleCollider2D>();

        ResetPoints();

        _player = transform.parent.GetComponent<Player>();
        _playerRb =  _player.GetComponent<Rigidbody2D>();
        _playerController = _player.GetComponent<CharacterController2D>();

        _initialGravityScale =  _playerRb.gravityScale;

        if(updateLengthEvent == null) updateLengthEvent = new UnityEvent();
        updateLengthEvent.AddListener(UpdateLine);
    }

    private void Update()
    {
        
        //_isMoving = _playerController.IsTryingToMove;
        //if(_isMoving) LineMovement();

        // if(Input.GetMouseButtonDown(1)) 
        // {
        //     _playerController.GetComponent<Animator>().SetTrigger("tongueTriggered");
        //     updateLengthEvent.Invoke();
        // }
        
        if(Input.GetMouseButton(1) && _incrementLengthValue) 
        {
            _playerRb.gravityScale = 1f;
            _playerRb.velocity = new Vector2 (0f, 0f);
            LineMovement();
        }

        else if(Input.GetMouseButtonUp(1))
        {
            _playerRb.gravityScale = _initialGravityScale;
            //_playerRb.velocity = new Vector2 (_playerRb.velocity.x, 0f);

            _incrementLengthValue = false;
            _decrementLengthValue = true; 
        }
        else if(_decrementLengthValue) LineMovement();
        
        if (_enemyFound) DragPlayer();

    }

    private void FixedUpdate()
    {
        if(DebugObject) DebugProperties(true);
    }

    private void UpdateLine()
    {
        ResetPoints();
        if(!_incrementLengthValue) _incrementLengthValue = true;
    }
    
    private void LineMovement() //strech the line renderer itself
    {
        
        bool facingRight = _playerController.IsMovingRight;
        int direction = facingRight ? 1 : -1;
        _lineLength = Mathf.Abs(_lineRenderer.GetPosition(1).x - _lineRenderer.GetPosition(0).x);


        if(_isMoving) 
        {
            _lineRenderer.SetPosition(0, _pivotObject.position);
            _endPointLine.x = _pivotObject.position.x + (_lineLength*direction);
            _endPointLine.y = _pivotObject.position.y;
            _lineRenderer.SetPosition(1, _endPointLine);    
        }

        if(!_enemyFound)
        {
            if(_incrementLengthValue && _lineLength <= _maxLength)
            {
                UpdateLength(direction, "Increment");

                _enemyFound = EnemyCheck(direction);
            }
            else if(_lineLength >= _maxLength && !_decrementLengthValue)
            {
                _incrementLengthValue = false;
                _decrementLengthValue = true;
            }
            else if(_decrementLengthValue && _lineLength > 0f ) 
            {
                UpdateLength(direction, "Decrement");

                if(_lineLength >= 0f && _lineLength <= 0.3f)
                {
                    ResetPoints();
                    _decrementLengthValue = false;
                } 
            }
        }
        
    }

    private void UpdateLength(int direction, string action)
    {
        if(action == "Increment")
        {
            _endPointLine.x += _speed * direction; 
            
        }
        else if(action == "Decrement")
        {
            _endPointLine.x -= _speed * direction; 
        }

        _lineRenderer.SetPosition(1, new Vector3 (_endPointLine.x, _endPointLine.y, _endPointLine.z));
    }

    private void ResetPoints()
    {
        _lineRenderer.SetPosition(0, _pivotObject.position);
        _lineRenderer.SetPosition(1, _pivotObject.position);
        _endPointLine = _lineRenderer.GetPosition(1);
    }

    private bool EnemyCheck(int direction)
    {
        Vector2 origin = (Vector2)transform.position;
        RaycastHit2D hit = Physics2D.Raycast(origin, Vector2.right*direction, _lineLength, _enemyCheckMask);

        // check for ground using origin position
        if(hit.collider != null)
        {
            Debug.Log("Enemy Found");
            EnemyFound = true;
            _enemyPosition.x = hit.collider.transform.position.x + (_enemyPositionOffset * direction);
            _enemyPosition.y = hit.collider.transform.position.y;
            return true;
        }

        return false;
    }

    private void DragPlayer()
    {
        //Debug.Log("Enemy Transform");
        //Debug.Log(_enemyPosition);

        ResetPoints();
        _player.transform.position = _enemyPosition;

        transform.parent.GetComponent<CharacterController2D>().TryImpulse(1f);

        _enemyFound = false;
        _incrementLengthValue = false;
        _decrementLengthValue = true;
    }

    public void DebugProperties(bool debugFlag)
    {
        DebugObject = debugFlag;
        Speed = _speed;
        MaxLength = _maxLength;
        LineLength = _lineLength;
        IsMoving = _isMoving;
        EnemyFound = _enemyFound;
    }
}


