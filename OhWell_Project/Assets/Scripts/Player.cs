﻿using UnityEngine;

public class Player : Unit
{
    [SerializeField, Header("Current Player State")]
    private PlayerStateBase _state;

    [SerializeField, Header("Tongue")]
    private LineRenderer _tongue;
    [SerializeField]
    private Transform _tonguePivot;
    [SerializeField]
    private Transform _tonguePivotEnd;
    [SerializeField]
    public LayerMask _grabbableCheckMask; //TODO GET RID OF THIS
    [SerializeField]
    private AimReticle _aimReticle;
    private Transform _eatableObject;

    [SerializeField, Header("Attack")]
    private GameObject _timedAttackSprite;

    [Header("Player Sound Settings")]
    public AudioClip[] audioTongue;
    public AudioClip[] audioJump;
    public AudioClip[] audioFootstepsRun;
    public AudioClip[] audioLanding;
    public AudioClip[] audioBounce;
    public AudioClip[] audioSword;

    
    [Header("Voice Over")]
    public AudioClip[] voJump;
    public AudioClip[] voAttack;
    public AudioClip[] voHurt;
    public AudioClip[] voDead;


    public PlayerStateBase State 
    {
        get 
        {
            return this._state;
        }
        set
        {
            if (_state!= null)
                this._state.End();
            this._state =  value;
            this._state.Initialize();
        }
    }

    public LineRenderer Tongue
    {
        get{return _tongue;}
    }
    public Transform TonguePivot
    {
        get{return _tonguePivot;}
    }
    public Transform TonguePivotEnd
    {
        get{return _tonguePivotEnd;}
    }
    public Rigidbody2D PlayerRb
    {
        get{return this.gameObject.GetComponent<Rigidbody2D>();}
    }
    public AimReticle AimReticle
    {
        get{return _aimReticle;}
    }
    public GameObject TimedAttackSprite
    {
        get{return _timedAttackSprite;}
    }
    public Transform EatableObject
    {
        get{return _eatableObject;}
        set{_eatableObject = value;}
    }

    private void Start()
    {
        int x = 1 << LayerMask.NameToLayer("Enemy");
        int y = 1 << LayerMask.NameToLayer("Mushroom");
        int z = 1 << LayerMask.NameToLayer("Fly");
        int w = 1 << LayerMask.NameToLayer("Pickup");
        _grabbableCheckMask = x | y | z | w;

        //The player begins in the Exploring State
        this.State = new ExploringState(this);
    }

    private void Update()
    {
        this._state.Update();
    }

    private void OnDrawGizmosSelected()
    {
        if (this._characterController != null)
        {
            if (this._characterController.IsMovingRight)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawWireSphere(this.transform.position + new Vector3(2.5f,2f,0f), 4f);
            }
            else
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawWireSphere(this.transform.position + new Vector3(-2.5f,2f,0f), 4f);
            }
        }
    }

    // override Death
    protected override void Death()
    {
        // call base function
        base.Death();
        
        //Play Death Animation
        AnimationClip deathClip = GetAnimationClip("Death");
        if (deathClip != null)
            _animator.Play(deathClip.name);

        // start GameOver
        GameManager.Instance.GameOver();
    }
}
