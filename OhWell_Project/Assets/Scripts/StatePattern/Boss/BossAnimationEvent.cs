﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAnimationEvent : MonoBehaviour
{
    //Working perfectly
    // DO NOT TOUCH!
    [Header("Boss Colliders")]
    [SerializeField]
    private CapsuleCollider2D _rightWingCollider;
    [SerializeField]
    private CapsuleCollider2D _leftWingCollider;
    [SerializeField]
    private CapsuleCollider2D _beakWingCollider;

    public void DisableCollider(string collider)
    {
        if(collider.ToUpper() == "RIGHT") _rightWingCollider.enabled = false;
        else if(collider.ToUpper() == "LEFT") _leftWingCollider.enabled = false;
        else  _beakWingCollider.enabled = false;
    }


    public void EnableCollider(string collider)
    {
        if(collider.ToUpper() == "RIGHT") _rightWingCollider.enabled = true;
        else if (collider.ToUpper() == "LEFT") _leftWingCollider.enabled = true;
        else _beakWingCollider.enabled = true;
    }
}
