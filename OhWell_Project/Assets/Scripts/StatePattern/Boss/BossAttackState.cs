﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAttackState : BossStateBase
{
    public BossAttackState(BossEnemy context) : base(context)
    {
    }

    public override void Initialize()
    {
        base.Initialize();
        Debug.Log("Boss entered Attack State");
        PlayRandomAttack();  
    }

    public override void Update()
    {
        base.Update();
    }

    public override void End()
    {
        base.End();
    }

    public void PlayRandomAttack()
    {
        int randomItzel = Random.Range(0,4);

        switch(randomItzel)
        {
            case 0:
                context.BossAnimator.Play("WingRight");
                break;
            case 1:
                context.BossAnimator.Play("WingLeft");
                break;
            case 2:
                context.BossAnimator.Play("BeakRight");
                break;
            case 3:
                context.BossAnimator.Play("BeakLeft");
                break;
            default:
                break;
        }
        
        context.State = new BossIdleState(this.context);
        
    }
}
