﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDeadState : BossStateBase
{
    //Camera Shake Parameters
    private CameraShake _cameraShake;
    private float _shakeAmount = 0.7f;
    private float _shakeLength = 0.5f;

    public BossDeadState(BossEnemy context) : base(context)
    { 
    }

    public override void Initialize()
    {
        base.Initialize();

        // Camera Shake handling
       // _cameraShake = GameManager.Instance.GetComponent<CameraShake>();
       // _cameraShake.Shake(_shakeAmount, _shakeLength);

        context.CallEndGame();
        Debug.Log("BOSS Entered the Dead State");
    }

    public override void Update()
    {
        base.Update();
    }

    public override void End()
    {
        base.End();
    }
}
