﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTakingDamageState : BossStateBase
{
    private float _timeEnteringTakingDamageState;
    private float _damagedAnimationLenght;
    private float _damageTaken;

    //Camera Shake Parameters
    private CameraShake _cameraShake;
    private float _shakeAmount = 0.7f;
    private float _shakeLength = 0.5f;

    public BossTakingDamageState(BossEnemy context, float damageReceived) : base(context)
    {
        _damageTaken = damageReceived;
    }

    public override void Initialize()
    {
        base.Initialize();
        Debug.Log("Boss entered Taking Damage State");

        _timeEnteringTakingDamageState = Time.time;

        context.Health.Damage(_damageTaken); 
        context.BossAnimator.Play("TakeHit"); 

        // Camera Shake handling
        _cameraShake = GameManager.Instance.GetComponent<CameraShake>();
        _cameraShake.Shake(_shakeAmount, _shakeLength);

        AnimationClip damagedClip =  context.GetAnimationClip("TakeHit");
        if (damagedClip != null)
        {
            context.UnitAnimator.Play(damagedClip.name);
            _damagedAnimationLenght = damagedClip.length;
        }
        
    }

    public override void Update()
    {
        base.Update();

        if (Time.time - _timeEnteringTakingDamageState >= _damagedAnimationLenght && context.Health.IsAlive)
        {
            this.context.State = new BossIdleState(this.context);
        }
    }

    public override void End()
    {
        base.End();
    }
}
