﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossPresentationState : BossStateBase
{
    private float _timeEnteredThisState;

    //Camera Shake Parameters
    private CameraShake _cameraShake;
    private float _shakeAmount = 0.7f;
    private float _shakeLength = 0.5f;

    public BossPresentationState(BossEnemy context) : base(context)
    {
    }

    public override void Initialize()
    {
        base.Initialize();
        Debug.Log("Boss entered Presentation State");

        // Camera Shake handling
        _cameraShake = GameManager.Instance.GetComponent<CameraShake>();
        _cameraShake.Shake(_shakeAmount, _shakeLength);

        // Camera.main.GetComponent<Camera>().orthographicSize = Mathf.Lerp(15f, 20f, Time.deltaTime);
        Camera.main.GetComponent<Camera>().orthographicSize = 20f;

        context.FollowCamera.SetBossAsTarget();

        context.BossAnimator.Play("TakeHit");
    }

    public override void Update()
    {
        base.Update();

        if (Time.time - _timeEnteredThisState >= 5f)
        {
            context.State = new BossIdleState(this.context);
        }

    }

    public override void End()
    {
        base.End();
        context.FollowCamera.SetPlayerAsTarget();
    }
}
