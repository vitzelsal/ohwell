﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossWaitingState : BossStateBase
{
    public BossWaitingState(BossEnemy context) : base(context)
    {
    }

    public override void Initialize()
    {
        base.Initialize();
    }

    public override void Update()
    {
        base.Update();
    }

    public override void End()
    {
        base.End();
    }
}
