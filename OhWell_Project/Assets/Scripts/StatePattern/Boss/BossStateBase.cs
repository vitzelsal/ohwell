﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BossStateBase
{
    protected BossEnemy context;

    //BossEnemy State Base constructor
    protected BossStateBase (BossEnemy context)
    {
        this.context = context;
    }
   
    public virtual void Initialize()
    {
    }

    public virtual void Update()
    {
         if (context.Health != null)
        {
            if (context.Health.IsAlive == false)
            {
                //change to dead state
                context.State = new BossDeadState(this.context);
                //end function early
                return;
            }
        }
    }

    public virtual void End() 
    {   

    }
}
