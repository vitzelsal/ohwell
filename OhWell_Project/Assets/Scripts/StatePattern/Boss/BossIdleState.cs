﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossIdleState : BossStateBase
{
    private float _timeEnteredIdle;

    private float _attackRate = 3f;

    public BossIdleState(BossEnemy context) : base(context)
    {
    }

    public override void Initialize()
    {
        base.Initialize();
        Debug.Log("Boss entered Idle State");

        context.BossAnimator.Play("Idle");
        _timeEnteredIdle = Time.time;
    }

    public override void Update()
    {
        base.Update();

        if (Time.time - _timeEnteredIdle >= _attackRate)
        {
            context.State = new BossAttackState(this.context);
        }
    }

    public override void End()
    {
        base.End();
    }
}
