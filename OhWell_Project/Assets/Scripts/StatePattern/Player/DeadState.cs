﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadState : PlayerStateBase
{
    public DeadState(Player context) : base(context)
    {
    }

    public override void Initialize()
    {
        base.Initialize();
        //fix collider
        var playerCollider = context.GetComponent<CapsuleCollider2D>();

        if (playerCollider != null) 
            playerCollider.size = new Vector2(playerCollider.size.x, 4.18f);
        
        Debug.Log("Entered the Dead State");
        context.UnitAnimator.Play("Death");
        
        //stop moving
        context.CharacterController.SetMoveInput(0f);
        context.Tongue.SetPosition(0, context.TonguePivot.position);
        context.Tongue.SetPosition(1, context.TonguePivot.position);
    }

    public override void Update()
    {
        base.Update();
    }

    public override void End()
    {
        base.End();
    }
}
