﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Commands
{
    public const string _moveInput = "Horizontal";
    public const string _verticalInput = "Vertical";
    public const string _jumpInput = "Jump";
    public const string _attackInput = "Fire1";
    public const string _tongueInput = "Fire2";
    public const string _pauseInput = "Cancel";
}
