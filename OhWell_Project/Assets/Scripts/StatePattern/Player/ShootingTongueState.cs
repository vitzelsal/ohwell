﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingTongueState : PlayerStateBase
{
    private Vector3 _endPointPosition;
    private float _tongueSpeed = 4f;
    //private float _tongueMaxLength = 10f;
    private int _direction = 1;

    private float _lineLength;
    private bool _grabbableFound;
    private Vector2 _aimDir;  
    private Vector3 _tongueMaximumLength;
    private float t;

    public ShootingTongueState(Player context) : base(context)
    {
    }
    
    public override void Initialize()
    {
        base.Initialize();
        Debug.Log("Entered the Shooting Tongue State");

        context.Tongue.GetComponent<LineRenderer>().enabled = true;

        //Movement
        context.CharacterController.SetMoveInput(0f);
        //context.PlayerRb.gravityScale =  0f;
        context.PlayerRb.velocity = Vector2.zero;

        //animation
        context.UnitAnimator.SetTrigger("tongueTriggered");

        //setting parameters
        context.Tongue.SetPosition(0, context.TonguePivot.position);
        context.Tongue.SetPosition(1, context.TonguePivot.position);
        _endPointPosition = context.Tongue.GetPosition(1);
        _direction = context.CharacterController.IsMovingRight ? 1 : -1;
        _aimDir = GrabAimDirections();
        _tongueMaximumLength = context.TonguePivotEnd.position - context.TonguePivot.position;

        // Sound
        context.audioPlayer.PlaySound(context.audioTongue);
    }

    public override void Update()
    {
        base.Update();

        context.Tongue.SetPosition(0, context.TonguePivot.position);

        // apply move input from player to character controller while shotting the tongue in the air
        float input = Input.GetAxis(Commands._moveInput);
        if (context.CharacterController != null)
        {   
            if (!context.CharacterController.IsGrounded)
                context.CharacterController.SetMoveInput(input);
        }

        _tongueMaximumLength = context.TonguePivotEnd.position - context.TonguePivot.position;
        var _directionVector = _tongueMaximumLength.normalized;

        RaycastHit2D hit = Physics2D.CircleCast(context.TonguePivot.position,0.6f, _directionVector, _tongueMaximumLength.magnitude, context._grabbableCheckMask);

        _lineLength = Mathf.Abs((context.Tongue.GetPosition(1) - context.Tongue.GetPosition(0)).magnitude);

        if (_lineLength <= _tongueMaximumLength.magnitude -0.2f)
        {
            _endPointPosition = Vector3.Lerp(context.TonguePivot.position, context.TonguePivotEnd.position, t);
            t += Time.deltaTime *_tongueSpeed;
            context.Tongue.SetPosition(1, _endPointPosition); 
        }
        else
        {
            if (!_grabbableFound)
            {
                this.context.State = new RetractTongueState(this.context);
            }
        }       

        //Enemy or Mushroom Found
        if(hit.collider != null)
        {
            _grabbableFound = true;
            if (hit.transform.gameObject.layer == 10) //Enemy
            {
                //this.context.Tongue.SetPosition(1, hit.transform.position + new Vector3(-1f * _direction, 3f, 0f)); //enemies need an offset, so the tongue doesn't go to the ground.
                //this.context.Tongue.SetPosition(1, hit.transform.position + new Vector3(-1f * _direction, 1f, 0f)); //enemies need an offset, so the tongue doesn't go to the ground.
                this.context.Tongue.SetPosition(1, hit.transform.GetComponent<Enemy>().PointToGrab.position + new Vector3(-1f*_direction, 0f, 0f));
                Enemy enemy = hit.transform.GetComponent<Enemy>();
                if (enemy != null)
                {
                    enemy.isTargeted = true;
                }

            }
            else if (hit.transform.gameObject.layer == 13) //Mushroom
            {
                this.context.Tongue.SetPosition(1, hit.transform.position);
                this.context.audioPlayer.PlaySound(this.context.audioBounce);
            }
            else if (hit.transform.gameObject.layer == 14) //Fly or Pickup
            {
                this.context.Tongue.SetPosition(1, hit.transform.position);
                context.EatableObject = hit.transform;
            }
            else if (hit.transform.gameObject.layer == 12) //Fly or Pickup
            {
                var orb = hit.transform.gameObject.GetComponent<Orb>();
                orb.audioPlayer.PlaySound(orb.audioOrbs);
                this.context.Tongue.SetPosition(1, hit.transform.position);
                context.EatableObject = hit.transform;
            }
            
            this.context.State = new GrabState(this.context, hit.transform.position, hit.transform.gameObject.layer);            
        }
    }

    public override void End()
    {
        base.End();
    }
}

// public override void Update()
//     {
//         base.Update();

//         var _directionVector = Vector2.right*_direction;
//         //_directionVector = Quaternion.Euler(0, 0, context.AimReticle.AngleForRaycast) * _directionVector;

//         //RaycastHit2D hit = Physics2D.Raycast(context.TonguePivot.position, Vector2.right*_direction, _tongueMaxLength *1.4f, context._grabbableCheckMask);
//         //RaycastHit2D hit = Physics2D.CircleCast(context.TonguePivot.position,0.5f, Vector2.right*_direction, _tongueMaxLength *1.4f, context._grabbableCheckMask);
//         // RaycastHit2D hit = Physics2D.CircleCast(context.TonguePivot.position,0.7f, _directionVector, _tongueMaxLength *1.4f, context._grabbableCheckMask);
//         RaycastHit2D hit = Physics2D.CircleCast(context.TonguePivot.position,0.6f, _directionVector, _tongueMaximumLength.magnitude, context._grabbableCheckMask);

//         // _lineLength = Mathf.Abs(context.Tongue.GetPosition(1).x - context.Tongue.GetPosition(0).x); //************TODO: NAO COMPARAR SOMENTE A POSICAO x!!!!!!!!
//         _lineLength = Mathf.Abs((context.Tongue.GetPosition(1) - context.Tongue.GetPosition(0)).magnitude);
        
//         if (_lineLength <= _tongueMaximumLength.magnitude)
//         {
//             //_endPointPosition.x += _tongueSpeed * _direction;
//             _endPointPosition = Vector3.Lerp(context.TonguePivot.position, context.TonguePivotEnd.position, _tongueSpeed*Time.deltaTime);
//             context.Tongue.SetPosition(1, _endPointPosition); 
//         }
//         else
//         {
//             if (!_enemyFound)
//             {
//                 this.context.State = new RetractTongueState(this.context);
//             }
//         }            

//         if(hit.collider != null) //&& _lineLength >= _tongueMaxLength
//         {
//             Debug.Log("Enemy or Mushroom Found");
//             _enemyFound = true;
//             this.context.Tongue.SetPosition(1, hit.transform.position);
//             this.context.State = new GrabState(this.context, hit.transform.position, hit.transform.gameObject.layer);            
//         }

//         if (Input.GetMouseButtonUp(1))
//         {
//             this.context.State = new RetractTongueState(this.context);
//         }

//     }
