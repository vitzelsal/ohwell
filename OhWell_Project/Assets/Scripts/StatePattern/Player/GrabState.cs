﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabState : PlayerStateBase
{
    private Vector3 _desiredPos;
    private int _grabbableLayer;
    private Vector3 _enemyPositionOffset = new Vector3(-3f, 1f, 0f);
    private int _direction;

    private float _dragVelocity = 15f;
    private float _maxDistanceToAttack = 4.5f;
    private float _impulseMagnitude = 30f;
    private float _impulseAfterAttackMagnitude = 10f;

    private float _distanceFromTarget;
    private float _currentDistanceFromTarget;
    private float _t;

    //Timed Attack variables
    private GameObject _timedAttackSprite;
    private bool _timedAttackSpriteExists;
    private bool _isSlowMotionActive;
    private float _timeEnteredInSlowMotionState;
    private float _attackGapTime = 0.25f;
    private bool _isTheFirstFrameOfGrabState;

    public GrabState(Player context, Vector3 desiredPos, int grabbableLayer) : base(context)
    {
        _desiredPos = desiredPos;
        _grabbableLayer = grabbableLayer;
    }

    public override void Initialize()
    {
        base.Initialize();
        Debug.Log("Entered the Grab State");
        _direction = context.CharacterController.IsMovingRight ? 1 : -1;
        _distanceFromTarget = Mathf.Abs((_desiredPos - context.gameObject.transform.position).magnitude);
        _isTheFirstFrameOfGrabState = true;
    }

    public override void Update()
    {
        base.Update();
        
        //***********************************************TIMED BASED ATTACK*****************************************************************************
        // 1 - Check if this is the first interation and if it is an enemy
        if (_isTheFirstFrameOfGrabState && _grabbableLayer == 10) 
        {
            // 2 - Calculate the current distance between the target/enemy and the player
            _currentDistanceFromTarget = Mathf.Abs((_desiredPos - context.gameObject.transform.position).magnitude);

            // 3 - When the distance calculated at 2 is half of the original one
            if (_currentDistanceFromTarget <= _distanceFromTarget/2f)
            {
                if (!_isSlowMotionActive)
                {
                    // 4 - Get the time the slow motion effect is activate
                    _timeEnteredInSlowMotionState = Time.time;
                }

                _isSlowMotionActive = true;
                

                // 5 - Intantiate ONE and just ONE timed attack sprite
                if (!_timedAttackSpriteExists)
                {
                    _timedAttackSprite = GameObject.Instantiate(context.TimedAttackSprite, context.Tongue.GetPosition(1), Quaternion.identity);
                    _timedAttackSpriteExists = true;
                }

                // 6 - Reduce the time scale
                Time.timeScale = 0.2f;

                //6.1 - Make the first point of the tongue continue following the player 
                context.Tongue.SetPosition(0, context.TonguePivot.position);

                // 7.1 - If the player doesn't do anything in the attackGapTime
                if (Time.time - _timeEnteredInSlowMotionState >= _attackGapTime)
                {
                    // 8.1 OUT OF SLOW MOTION
                    Time.timeScale = 1f;
                    _isSlowMotionActive = false;
                    GameObject.Destroy(_timedAttackSprite);
                    _timedAttackSpriteExists = false;
                    _isTheFirstFrameOfGrabState = false;
                }
                // 7.2 - If the player press the attack button in the attackGapTime
                else
                {
                    if (Input.GetButtonDown(Commands._attackInput))
                    {              
                        //8.2 OUT OF SLOW MOTION
                        Time.timeScale = 1f;
                        _isSlowMotionActive = false;
                        GameObject.Destroy(_timedAttackSprite);
                        _timedAttackSpriteExists = false;
                        _isTheFirstFrameOfGrabState = false;

                        Vector2 aimDir = GrabAimDirections();
                        context.PlayerRb.AddForce(new Vector2(aimDir.x*_impulseAfterAttackMagnitude/2, aimDir.y * _impulseAfterAttackMagnitude/2), ForceMode2D.Impulse);
                        context.State = new AttackingState(this.context);
                    }
                }
            }
        }
        //********************************************************************************************************************************************

        if (!_isSlowMotionActive && _grabbableLayer != 14 && _grabbableLayer != 12) // It is not a fly or a pickup
        {
            context.transform.position = Vector3.Lerp(context.transform.position, _desiredPos + (new Vector3(_enemyPositionOffset.x *_direction, _enemyPositionOffset.y, _enemyPositionOffset.z)), _t);
            _t += _dragVelocity * Time.deltaTime/10f;
            context.Tongue.SetPosition(0, context.TonguePivot.position);
        }

        if (_grabbableLayer == 10)//Enemy
        {
            if (Mathf.Abs(context.Tongue.GetPosition(1).x - context.Tongue.GetPosition(0).x) >= _maxDistanceToAttack - 2.5f || Mathf.Abs(context.Tongue.GetPosition(1).y - context.Tongue.GetPosition(0).y) >= _maxDistanceToAttack - 2.5f)
            {
                if (Input.GetButtonDown(Commands._attackInput))
                {                
                    Vector2 aimDir = GrabAimDirections();
                    context.PlayerRb.AddForce(new Vector2(aimDir.x*_impulseAfterAttackMagnitude/2, _impulseAfterAttackMagnitude*2f), ForceMode2D.Impulse);
                    context.State = new AttackingState(this.context);
                }
            }
            else
            {
                context.State =  new TakingDamageState(this.context, 15f, 15f, 0f, false); // the damage itself will come because you touch the enemy;
            } 
        }

        if (_grabbableLayer == 13) //Mushroom
        {
            if (Mathf.Abs(context.Tongue.GetPosition(1).x - context.Tongue.GetPosition(0).x) >= _maxDistanceToAttack || Mathf.Abs(context.Tongue.GetPosition(1).y - context.Tongue.GetPosition(0).y) >= _maxDistanceToAttack)
            {
            }
            else
            {
                Vector2 aimDir = GrabAimDirections();
                context.PlayerRb.AddForce(new Vector2(aimDir.x*_impulseMagnitude/2, _impulseMagnitude*1.2f), ForceMode2D.Impulse);
                context.State = new ExploringState(this.context);
            } 
        }
        if (_grabbableLayer == 14 || _grabbableLayer == 12) //Fly or Pickup
        {
            context.Tongue.SetPosition(0, context.TonguePivot.position);
            context.State = new RetractTongueState(this.context);
        }
    }

    public override void End()
    {
        base.End();
        //OUT OF SLOW MOTION
        Time.timeScale = 1f;
        _isSlowMotionActive = false;
        GameObject.Destroy(_timedAttackSprite);
        _timedAttackSpriteExists = false;
        _isTheFirstFrameOfGrabState = false;
    }
}
