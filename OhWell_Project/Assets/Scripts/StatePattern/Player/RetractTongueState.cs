﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RetractTongueState : PlayerStateBase
{
    private Vector3 _endPointPosition = new Vector3();
    private float _tongueRetractSpeed = 17.5f;
    private int _direction = 1;

    public RetractTongueState(Player context) : base(context)
    {
    }

    public override void Initialize()
    {
        base.Initialize();        
        Debug.Log("Entered the Retract Tongue State");
        _endPointPosition = context.Tongue.GetPosition(1);
        _direction = context.CharacterController.IsMovingRight ? 1 : -1;
        context.PlayerRb.gravityScale =  5f;
    }

    public override void Update()
    {
        base.Update();

        if (context.EatableObject != null)
        {
            context.EatableObject.position = context.Tongue.GetPosition(1);
        }

        context.Tongue.SetPosition(0, context.TonguePivot.position);

        //var lineLength = Mathf.Abs(context.Tongue.GetPosition(1).x - context.Tongue.GetPosition(0).x);
        
        _endPointPosition = Vector3.Lerp(context.Tongue.GetPosition(1), context.Tongue.GetPosition(0), _tongueRetractSpeed * Time.deltaTime);
        context.Tongue.SetPosition(1, _endPointPosition);

        if (Mathf.Abs((context.Tongue.GetPosition(1) - context.Tongue.GetPosition(0)).magnitude) <= 1f)
        {
            this.context.State = new ExploringState(this.context);
        }
    }

    public override void End()
    {
        base.End();
        context.Tongue.SetPosition(0, context.TonguePivot.position);
        context.Tongue.SetPosition(1, context.TonguePivot.position);
        context.Tongue.GetComponent<LineRenderer>().enabled = false;
    }
}
    // public override void Update()
    // {
    //     base.Update();

    //     //var lineLength = Mathf.Abs(context.Tongue.GetPosition(1).x - context.Tongue.GetPosition(0).x);
        
    //     _endPointPosition = Vector3.Lerp(context.Tongue.GetPosition(1), context.Tongue.GetPosition(0), _tongueRetractSpeed * Time.deltaTime);
    //     context.Tongue.SetPosition(1, _endPointPosition);

    //     if (Mathf.Abs(_endPointPosition.x - context.Tongue.GetPosition(0).x) <= 1f)
    //     {
    //         this.context.State = new ExploringState(this.context);
    //     }
    // }
