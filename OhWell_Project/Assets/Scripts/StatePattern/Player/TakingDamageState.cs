﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakingDamageState : PlayerStateBase
{
    private float _timeEnteringTakingDamageState;
    private float _damagedAnimationLenght;
    private float _pushForceHorizontal;
    private float _pushForceVertical;
    private float _damageTaken;
    private bool _canMove;

    //Camera Shake Parameters
    private CameraShake _cameraShake;
    private float _shakeAmount = 0.2f;
    private float _shakeLength = 0.2f;

    public TakingDamageState(Player context, float pushForceMagnitudeHorizontal, float pushForceMagnitudeVertical, float damageReceived, bool canMove) : base(context)
    {
        this._pushForceHorizontal = pushForceMagnitudeHorizontal;
        this._pushForceVertical = pushForceMagnitudeVertical;
        this._damageTaken = damageReceived;
        this._canMove = canMove;
    }

    public override void Initialize()
    {
        base.Initialize();
        Debug.Log("Entered the Taking Damage State");

        // Camera Shake handling
        _cameraShake = GameManager.Instance.GetComponent<CameraShake>();
        _cameraShake.Shake(_shakeAmount, _shakeLength);

        _timeEnteringTakingDamageState = Time.time;
        
        float direction = context.CharacterController.IsMovingRight? 1f:-1f;
        context.PlayerRb.velocity = Vector2.zero;
        //context.PlayerRb.velocity = new Vector2(-direction, 0f);

        context.PlayerRb.AddForce(new Vector2(-_pushForceHorizontal*direction, _pushForceVertical), ForceMode2D.Impulse);
        context.Health.Damage(_damageTaken);

        //handle tongue position
        context.Tongue.SetPosition(0, context.TonguePivot.position);
        context.Tongue.SetPosition(1, context.TonguePivot.position);

        //Play Damaged Animation
        AnimationClip damagedClip =  context.GetAnimationClip("Damaged");
        if (damagedClip != null)
        {
            context.UnitAnimator.Play(damagedClip.name);
            _damagedAnimationLenght = damagedClip.length;
        }
    }

    public override void Update()
    {
        base.Update();
        
        if (Time.time - _timeEnteringTakingDamageState >= _damagedAnimationLenght)
        {
            this.context.State = new ExploringState(this.context);
        }

        if (_canMove)
        {
            // apply move input from player to character controller
            float input = Input.GetAxis(Commands._moveInput);

            if (context.CharacterController != null)
            {
                context.CharacterController.SetMoveInput(input);
            }

            // use the tongue
            if (Input.GetButtonDown(Commands._tongueInput))
            {
                this.context.State = new ShootingTongueState(this.context);
            }
        }
        else
        {
            context.CharacterController?.SetMoveInput(0f);
        }
    }

    public override void End()
    {
        base.End();
    }
}
