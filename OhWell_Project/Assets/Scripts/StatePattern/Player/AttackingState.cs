﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackingState : PlayerStateBase
{
    private float _timeAttackIsTriggered;
    private float _attackRate =  0.2f; // player can only attack one time on each 0.2 second
    private float _waitingToNextAttackTime = 0.4f; // maximum time the state will wait before going back to the exploring state
    private float _attackImpulseMagnitude = 40f;

    //Camera Shake Parameters
    private CameraShake _cameraShake;
    private float _shakeAmount = 0.1f;
    private float _shakeLength = 0.2f;

    public AttackingState(Player context) : base(context)
    {
    }

    public override void Initialize()
    {
        base.Initialize();
        Debug.Log("Entered the Attacking State");
        context.CharacterController.SetMoveInput(0f); //stop moving
        _timeAttackIsTriggered = Time.time;

        _cameraShake = GameManager.Instance.GetComponent<CameraShake>();
        
        Attack();
        Reset();
    }

    public override void Update()
    {
        base.Update();

        if (Input.GetButtonDown(Commands._attackInput))
        {
            if (Time.time - _timeAttackIsTriggered >= _attackRate)
            {
                Attack();
                _timeAttackIsTriggered = Time.time;
            }
        }

        if (Time.time - _timeAttackIsTriggered >= _waitingToNextAttackTime)
        {
            this.context.State = new ExploringState(this.context);
        }
    }

    public override void End()
    {
        base.End();
    }

    private void Attack()
    {
        // //shake camera
        _cameraShake.Shake(_shakeAmount/3f, _shakeLength/1.5f);

        context.GetComponent<AnimationAudioEvent>().PlayAttack();
        context.UnitAnimator.SetTrigger("attackTriggered");

        if (context.CharacterController.IsGrounded)
        {
            Vector2 attackImpulse = context.CharacterController.IsMovingRight ? new Vector2(_attackImpulseMagnitude, 0f) : new Vector2 (-_attackImpulseMagnitude, 0f);
            context.transform.gameObject.GetComponent<Rigidbody2D>().AddForce(attackImpulse, ForceMode2D.Impulse);
        }

        Collider2D[] objectsAttacked = context.CharacterController.IsMovingRight ? Physics2D.OverlapCircleAll(this.context.transform.position + new Vector3(2.5f,2f,0f), 4f) : 
                                                                                   Physics2D.OverlapCircleAll(this.context.transform.position + new Vector3(-2.5f,2f,0f), 4f);

        if (objectsAttacked != null)
        {
            for (int i = 0; i < objectsAttacked.Length; i++)
            {
                // get possible health component
                Health health = objectsAttacked[i].GetComponent<Health>();

                //check if health exists
                if (health != null)
                {
                    //Debug.Log(objectsAttacked[i].gameObject.name);
                    //check if health is enemy
                    if (health.Team != context.Health.Team)
                    {
                        //shake camera
                        _cameraShake.Shake(_shakeAmount, _shakeLength);
                        health.Damage(100f);
                    }
                }                  
             }
        }
    }
}
