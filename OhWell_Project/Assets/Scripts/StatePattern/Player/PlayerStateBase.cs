﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerStateBase
{
    protected Player context;
    private int _direction = 1;

    //Player State Base constructor
    protected PlayerStateBase (Player context)
    {
        this.context = context;
    }

    //INITIALIZE: works as the Start function that runs when player enter a state
    public virtual void Initialize ()
    {
    }

    //UPDATE: works as the Update function for a state
    public virtual void Update()
    {
        //Animator parameters set up
        context.UnitAnimator.SetBool("isOnGround", context.CharacterController.IsGrounded);
        context.UnitAnimator.SetFloat("VerticalSpeed", context.PlayerRb.velocity.y);

        _direction = context.CharacterController.IsMovingRight ? 1:-1;
        // var startPos =  context.TonguePivot.transform.position;
        // var endPos = context.TonguePivot.transform.position + new Vector3 (10f, 0f,0f);
        // var testVector = new Vector3(context.TonguePivot.position.x + (14*_directionInt) , context.TonguePivot.position.y, context.TonguePivot.position.z);
        // Debug.DrawLine(context.TonguePivot.position, testVector, Color.red);

        Debug.DrawLine(context.TonguePivot.position, context.TonguePivotEnd.position, Color.blue);

        if (context.Health != null)
        {
            if (context.Health.IsAlive == false)
            {
                //change to dead state
                context.State = new DeadState(this.context);
                //end function early
                return;
            }
        }
    }

    //END: Code that will be running when player leave a state
    public virtual void End()
    {
    }

    protected void Reset()
    {
        context.Tongue.SetPosition(0, context.TonguePivot.position);
        context.Tongue.SetPosition(1, context.TonguePivot.position);
        context.PlayerRb.gravityScale =  5f;
    }

    protected Vector2 GrabAimDirections()
    {
        float horizontalInput = Input.GetAxis(Commands._moveInput);
        float verticalInput = Input.GetAxis(Commands._verticalInput);
        Vector2 aimDirection = new Vector2(horizontalInput, verticalInput);
        aimDirection = aimDirection.normalized;

        return aimDirection;
    }
}
