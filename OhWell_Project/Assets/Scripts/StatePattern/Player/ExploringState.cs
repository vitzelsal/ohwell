﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExploringState : PlayerStateBase
{
    private float _timeJumpButtonIsPressed;

    public ExploringState(Player context) : base(context)
    {
    }

    public override void Initialize()
    {
        base.Initialize();
        Reset();
        Debug.Log("Entered the Exploring State");
    }

    public override void Update()
    {
        base.Update();

        // apply move input from player to character controller
        float input = Input.GetAxis(Commands._moveInput);

        if (context.CharacterController != null)
        {
            context.CharacterController.SetMoveInput(input);
        }
        
        // jump
        if(Input.GetButtonDown(Commands._jumpInput))
        {
            _timeJumpButtonIsPressed = Time.time;
            context.UnitAnimator.SetTrigger("jumpTriggered");
            context.CharacterController.TryJump();
        }

        // use the sword
        if (Input.GetButtonDown(Commands._attackInput) && Time.time - _timeJumpButtonIsPressed >= 0.2f)
        {
            this.context.State =  new AttackingState(this.context);
        } 

        // use the tongue
        if (Input.GetButtonDown(Commands._tongueInput))
        {
            this.context.State = new ShootingTongueState(this.context);
        }

        if (Time.time - _timeJumpButtonIsPressed >= 0.4f)
        {
            _timeJumpButtonIsPressed = 0f;
        }
    }

    public override void End()
    {
        base.End();
    }
}
