﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadCollider : MonoBehaviour
{
    [SerializeField]
    private BossEnemy _BossEnemy;

    [SerializeField]
    private float _damage = 34f;

    private void OnTriggerEnter2D(Collider2D other)
    {
        var player = other.GetComponent<Player>();

        if (player != null)
            _BossEnemy.State = new BossTakingDamageState(_BossEnemy, _damage); 
    }
}
