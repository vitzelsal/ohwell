﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossEnemy : Unit
{
    [SerializeField, Header("Current Boss Enemy State")]
    private BossStateBase _state;

    [SerializeField]
    private FollowCamera _followCamera;

    [SerializeField]
    private GameObject _endGamePanel;

    [Header("Boss Enemy Sound Settings")]
    public AudioClip[] audioWings;
    public AudioClip[] audioIdle;
    public AudioClip[] audioBeak;

    private Animator _bossAnimator;
    // This makes NO SENSE
    public Animator BossAnimator 
    {
        get{return _bossAnimator;}
        set
        {
            _bossAnimator = value;
        }
    }

    public BossStateBase State
    {
        get {return _state;}
        set
        {
            if(_state != null) 
                _state.End();

            _state = value;
            _state.Initialize();
        }
    }

    public FollowCamera FollowCamera
    {
        get {return _followCamera;}
        set { _followCamera = value;}
    }

    private void Awake()
    {
        _bossAnimator = gameObject.GetComponent<Animator>();
        _health = GetComponent<Health>();
        _health.DeathEvent.AddListener(Death);
    }

    private void Start()
    {
        this.State = new BossWaitingState(this);
    }

    private void Update()
    {
        this._state.Update();
    }


    protected override void Death()
    {
        base.Death();

        if (_bossAnimator != null)
        {
            _bossAnimator.SetTrigger("deathTrigger");
        }
        Destroy(this.gameObject, 10f);
    }

    public void CallEndGame()
    {
        _endGamePanel.GetComponent<Animator>().enabled = true;
        StartCoroutine("GoToMenu");
    }

    IEnumerator GoToMenu()
    {
        yield return new WaitForSeconds(8f);
        SceneManager.LoadScene("Menu");
    }
}
