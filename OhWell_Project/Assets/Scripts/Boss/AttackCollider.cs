﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackCollider : MonoBehaviour
{
   private void OnTriggerEnter2D(Collider2D other)
   {
        var player = other.GetComponent<Player>();

        if (player != null)
             player.State = new TakingDamageState(player, 45f,  10f, 34f, false);  
   }
}
