﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAreaCollider : MonoBehaviour
{
    [SerializeField]
    private BossEnemy _bossEnemy;

    private void OnTriggerEnter2D(Collider2D other)
    {
        var player = other.GetComponent<Player>();

        if (player != null)
        {
            _bossEnemy.State = new BossPresentationState(_bossEnemy);
        }
    }
    
    private void OnTriggerExit2D(Collider2D other)
    {
        gameObject.SetActive(false);
    }
    
}
