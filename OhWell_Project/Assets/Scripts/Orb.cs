﻿using UnityEngine;

public class Orb : MonoBehaviour
{
    [Header("Sound Settings")]
    public GenericAudioPlayer audioPlayer;
    public AudioClip[] audioOrbs;

     private void OnTriggerEnter2D(Collider2D other)
    {
        // get possible player
        Player hitPlayer = other.GetComponent<Player>();

        if (hitPlayer != null)
        {
            OnOrbCollected();
        }
    }

    public void OnOrbCollected ()
    {
        GameManager.Instance.AddScore(1);
        this.gameObject.SetActive(false);
        audioPlayer.PlaySound(audioOrbs, this.gameObject);
        Destroy(this.gameObject, 2f);
    }
}
