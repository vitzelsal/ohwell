﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAnimationAudioEvent : MonoBehaviour
{
    private BossEnemy _bossEnemy;
    public static float _screamProbability = 0.35f;

    private void Start()
    {
        _bossEnemy = GetComponent<BossEnemy>();
    }

    public void PlayBossIdle()
    {
        if (Random.value <= AnimationAudioEvent._screamProbability) _bossEnemy.audioPlayer.PlaySound(_bossEnemy.audioIdle);
    }

    public void PlayBossWings()
    {
        _bossEnemy.audioPlayer.PlaySound(_bossEnemy.audioWings);
    }

    public void PlayBossBeak()
    {
        _bossEnemy.audioPlayer.PlaySound(_bossEnemy.audioBeak);
    }

}
