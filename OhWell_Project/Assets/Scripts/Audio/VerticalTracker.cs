﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class VerticalTracker : MonoBehaviour
{
    [SerializeField]
    private Player _player;

    [SerializeField, Header("Audio")]
    private AudioMixer _mixer;

    [SerializeField]
    private float _maxWaterVolume = 0.0f;

    [SerializeField]
    private float _initialVolume = -9.0f;

    [SerializeField]
    private float _multiplier = 1.0f;

    [SerializeField]
    private float _initialDistance = 0f;
    private float _currentDistance = 0f;

    [SerializeField]
    private PoisonedWater _poisonWater;

    private void Start()
    {
        _initialDistance = _player.transform.position.y - gameObject.transform.position.y;
    }

    private void Update()
    {
        if (_poisonWater != null)
        {
            if(_poisonWater.IsWaterTriggered)
                _maxWaterVolume = 2f;
            else
                _maxWaterVolume = 0f; 
        }

        float currentVolume;
        float distance = _player.transform.position.y - gameObject.transform.position.y;
        if (distance <= 0) 
            distance = 0.1f;
        float variation;
        if (_poisonWater.IsWaterTriggered)
            variation = Mathf.Abs(_initialVolume/distance)*6f;
        
        else
            variation = Mathf.Abs(_initialVolume/distance);

        _mixer.GetFloat("waterVolume", out currentVolume);
        float newVolume = _initialVolume + variation;
        if(newVolume > _maxWaterVolume) 
            _mixer.SetFloat("waterVolume", Mathf.Lerp(currentVolume, _maxWaterVolume, Time.deltaTime));
        else 
            _mixer.SetFloat("waterVolume", Mathf.Lerp(currentVolume, _initialVolume + variation, Time.deltaTime));
        
    
    }


    /*
        float distance = _player.transform.position.y - gameObject.transform.position.y;
        if (distance <= 0) distance = 0.1f;

        Debug.Log($"Initial distance {_player.transform.position.y - gameObject.transform.position.y}");
        Debug.Log($"Distance: {distance}");

        Debug.Log($"Initial Volume: {_initialVolume}");

        float variation = Mathf.Abs(_initialVolume/distance);

        Debug.Log($"Variation: {variation}");

        _mixer.SetFloat("waterVolume", _initialVolume + variation);

        float currentVolume;
        _mixer.GetFloat("waterVolume", out currentVolume);
         Debug.Log($"Current Volume: {currentVolume}");
        if(currentVolume > -6.0f) _mixer.SetFloat("waterVolume", _maxWaterVolume);
    */
}
