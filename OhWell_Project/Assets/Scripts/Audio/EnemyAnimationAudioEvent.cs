﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimationAudioEvent : MonoBehaviour
{
    private Enemy _enemy;

    private void Start()
    {
        _enemy = GetComponent<Enemy>();
    }

    public void PlayEnemyFootsteps()
    {
        _enemy.audioPlayer.PlaySound(_enemy.audioFootsteps);
    }

    public void PlayGroundEnemyIdle()
    {
         _enemy.audioPlayer.PlaySound(_enemy.audioIdle);
    }

    public void PlayGroundEnemyDead()
    {
         _enemy.audioPlayer.PlaySound(_enemy.audioDead);
    }

  
}
