﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MixerLevels : MonoBehaviour {

    [Range(-80f, 0f)]
    public float volume = 0f;

    [SerializeField]
    Slider sfxSlider;
    [SerializeField]
    Slider musicSlider;
    [SerializeField]
    Slider waterVolumeSlider;

    [SerializeField]
    AudioMixer _mixer;
    
    public void SetSfxLvl()
	{
        _mixer.SetFloat("sfxVolume", AudioConversionUtilities.linearToDecibel(sfxSlider.value));  
    }

    public void SetMusicLvl()
	{
        _mixer.SetFloat("musVolume", AudioConversionUtilities.linearToDecibel(musicSlider.value));  
    }

    public void SetWaterVolumeLvl()
    {
        _mixer.SetFloat("waterVolume", AudioConversionUtilities.linearToDecibel(waterVolumeSlider.value));  
    }

}
