﻿using UnityEngine;

public class AnimationAudioEvent : MonoBehaviour {

    private Player _player;
    private Enemy _enemy;
    private BossEnemy _bossEnemy;
    private Fly _fly;
    public static float _voiceProbability = 0.33f;
    public static float _screamProbability = 0.35f;

    private void Start()
    {
        _player = GetComponent<Player>();
        _enemy = GetComponent<Enemy>();
        _bossEnemy = GetComponent<BossEnemy>();
        _fly = GetComponent<Fly>();
    }

    public void PlayJumpVO()
    {
        //Play jumo VO
        if (Random.value <= AnimationAudioEvent._voiceProbability) _player.audioPlayer.PlaySound(_player.voJump);
        _player.audioPlayer.PlaySound(_player.audioJump);
    }

    public void PlayWalk()
    {
        //Play walk footsteps
        _player.audioPlayer.PlaySound(_player.audioFootsteps);
    }

    public void PlayRun()
    {
        _player.audioPlayer.PlaySound(_player.audioFootstepsRun);
    }

    public void PlayLand()
    {
        _player.audioPlayer.PlaySound(_player.audioLanding);
    }

    public void PlayHurt()
    {
        if (Random.value <= AnimationAudioEvent._voiceProbability) _player.audioPlayer.PlaySound(_player.voHurt);
    }

    public void PlayDead()
    {
        _player.audioPlayer.PlaySound(_player.voDead);
    }

    public void PlayEnemyFootsteps()
    {
        _enemy.audioPlayer.PlaySound(_enemy.audioFootsteps);
    }

    public void PlayGroundEnemyIdle()
    {
         _enemy.audioPlayer.PlaySound(_enemy.audioIdle);
    }

    public void PlayGroundEnemyDead()
    {
         _enemy.audioPlayer.PlaySound(_enemy.audioDead);
    }

    public void PlayAttack()
    {
        if (Random.value <= AnimationAudioEvent._voiceProbability) _player.audioPlayer.PlaySound(_player.voAttack);
        _player.audioPlayer.PlaySound(_player.audioSword);
    }

    public void PlayBossIdle()
    {
        Debug.Log("PLAY!!");
        _bossEnemy.audioPlayer.PlaySound(_bossEnemy.audioIdle);
    }

    public void PlayBossWings()
    {
        _bossEnemy.audioPlayer.PlaySound(_bossEnemy.audioWings);
    }

    public void PlayBossBeak()
    {
        _bossEnemy.audioPlayer.PlaySound(_bossEnemy.audioBeak);
    }

    public void PlayBuzz()
    {
        _fly.audioPlayer.PlaySound(_fly.audioIdle);
    }
}

