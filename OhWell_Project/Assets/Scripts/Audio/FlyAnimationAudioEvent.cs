﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyAnimationAudioEvent : MonoBehaviour
{
    private Fly _fly;

    private void Start()
    {
        _fly = GetComponent<Fly>();
    }

    public void PlayBuzz()
    {
        _fly.audioPlayer.PlaySound(_fly.audioIdle);
    }
}
