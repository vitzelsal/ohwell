﻿using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]
public class GenericAudioPlayer : MonoBehaviour
{
    [Header("Audio Source Settings")]
    [Tooltip("Audio Source to contain and manage audio clips.")]
    [SerializeField]
    private AudioSource audioSource = null;
    [SerializeField]
    private bool playDettached = false;

    // In Audio, 1.0f = original
    [Header("Clip Randomization")]
    [SerializeField]
    private bool randomizeVolume = false;
    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float maxVolume = 1.0f;
    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float minVolume = 1.0f;
    [SerializeField]
    private bool randomizePitch = false;
    [SerializeField]
    [Range(-3.0f, 3.0f)]
    private float maxPitch = 1.0f;
    [SerializeField]
    [Range(-3.0f, 3.0f)]
    private float minPitch = 1.0f;

    [Header("Audio Mixer")]
    [SerializeField]
    private AudioMixerGroup audioMixer = null;

    [Header("Mixer Snapshots")]
    [SerializeField]
    private AudioMixerSnapshot[] snapshots = {};
    [SerializeField]
    private float[] weights = {};
    [SerializeField]
    private float transitionTime = 1.0f;

    private void SetReferences()
    {
        audioSource = audioSource ?? GetComponent<AudioSource>();
        audioMixer = audioMixer ?? GetComponent<AudioMixerGroup>();
    }

    [ContextMenu("Trigger Sound")]
    public void PlaySound(AudioClip[] audioClips, GameObject gameObjectReference = null)
    {
        // Debug
        // Debug.Log($"Playing sound from: {this.gameObject.ToString()}");

        // Randomize Volume
        if (randomizeVolume)
        {
            float randomVolume = Random.Range(minVolume, maxVolume);
            audioSource.volume = randomVolume;
            //Debug.Log($"{gameObject.ToString()} | Volume at {randomVolume}");
        }

        // Randomize Pitch
        if (randomizePitch)    
        {
            float randomPitch = Random.Range(minPitch, maxPitch);
            audioSource.pitch = randomPitch;
            //Debug.Log($"{gameObject.ToString()} | Pitch at {randomPitch}");
        }

        // Randomize AudioClip
        {
            int randomClip = Random.Range(0, audioClips.Length);
            if (playDettached && gameObjectReference != null) AudioSource.PlayClipAtPoint(audioClips[randomClip], gameObjectReference.transform.position);
            else if (audioSource.loop) audioSource.clip = audioClips[randomClip];
            else audioSource.PlayOneShot(audioClips[randomClip]);
          //Debug.Log($"{gameObject.ToString()} | Clip #{randomClip}");
        }
    }
}
