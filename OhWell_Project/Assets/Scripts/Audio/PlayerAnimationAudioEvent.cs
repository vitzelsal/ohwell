﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationAudioEvent : MonoBehaviour
{
    private Player _player;
    public static float _voiceProbability = 0.33f;

    private void Start()
    {
        _player = GetComponent<Player>();
    }

    public void PlayJumpVO()
    {
        //Play jumo VO
        if (Random.value <= AnimationAudioEvent._voiceProbability) _player.audioPlayer.PlaySound(_player.voJump);
        _player.audioPlayer.PlaySound(_player.audioJump);
    }

    public void PlayWalk()
    {
        //Play walk footsteps
        _player.audioPlayer.PlaySound(_player.audioFootsteps);
    }

    public void PlayRun()
    {
        _player.audioPlayer.PlaySound(_player.audioFootstepsRun);
    }

    public void PlayLand()
    {
        _player.audioPlayer.PlaySound(_player.audioLanding);
    }

    public void PlayHurt()
    {
        if (Random.value <= AnimationAudioEvent._voiceProbability) _player.audioPlayer.PlaySound(_player.voHurt);
    }

    public void PlayDead()
    {
        _player.audioPlayer.PlaySound(_player.voDead);
    }

    public void PlayAttack()
    {
        if (Random.value <= AnimationAudioEvent._voiceProbability) _player.audioPlayer.PlaySound(_player.voAttack);
        _player.audioPlayer.PlaySound(_player.audioSword);
    }

}
