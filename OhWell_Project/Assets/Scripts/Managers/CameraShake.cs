﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public Camera mainCamera;

    [SerializeField] 
    private float _shakeAmount = 0f;

    private void Awake()
    {
        if (mainCamera == null)
        {
            mainCamera =  Camera.main;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            Shake(0.1f, 0.2f);
        }
    }

    public void Shake (float shakeAmount, float shakeLenght)
    {   
        _shakeAmount = shakeAmount;
        InvokeRepeating("DoShake", 0f, 0.01f);
        Invoke("StopShake", shakeLenght);
    }

    private void DoShake ()
    {
        if (_shakeAmount > 0f)
        {
            Vector3 camPos = mainCamera.transform.position;
            
            float offsetX = Random.value * _shakeAmount *2 - _shakeAmount;
            float offsetY = Random.value * _shakeAmount *2 - _shakeAmount;
            camPos.x += offsetX;
            camPos.y += offsetY;

            mainCamera.transform.position = camPos;
        }
    }

    private void StopShake ()
    {
        CancelInvoke("DoShake");
        mainCamera.transform.localPosition = Vector3.zero;
    }
}
