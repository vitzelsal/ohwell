﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointManager : MonoBehaviour
{
    public static CheckpointManager _checkPointManagerInstance;

    public int currentCheckPointId = 0;
    public Vector3 currentCheckPointPosition;
    public int scoreAtCurrentCheckpoint;
    public bool IsTriggeringWater;

    private void Awake()
    {
        if (_checkPointManagerInstance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _checkPointManagerInstance = this;
        }

        DontDestroyOnLoad(this.gameObject);
    }

    public void AddNewCheckpoint(Checkpoint checkpoint)
    {
        currentCheckPointId = checkpoint.Id;
        currentCheckPointPosition = checkpoint.RespawnPosition;
        scoreAtCurrentCheckpoint = GameManager.Instance.Score;
        IsTriggeringWater = checkpoint.TriggerWater;
    }
}
