﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Audio;

public class PauseManager : MonoBehaviour {

    public Player context;
    [SerializeField] 
    private GameObject _pauseScreen;
	[SerializeField] 
    AudioMixerSnapshot _snapshotPaused;
	[SerializeField]
     AudioMixerSnapshot _snapshotUnpaused;
	
	void Update()
	{
		if (Input.GetButtonDown(Commands._pauseInput) && context.Health.IsAlive) // TODO: Change to commands!
		{
			Pause();
		}
	}
	
	public void Pause()
	{
        //Stop time if it's running, start it back up if it's stopped
		EventSystem.current.SetSelectedGameObject(null);
        _pauseScreen.SetActive(!_pauseScreen.activeSelf);
		Time.timeScale = Time.timeScale == 0 ? 1 : 0;
		PauseAudio ();
	}
	
	void PauseAudio()
	{
		if (Time.timeScale == 0)
		{
            _snapshotPaused.TransitionTo(0f);
        }
        else
		{
           _snapshotUnpaused.TransitionTo(0f);
        }
    }
}
