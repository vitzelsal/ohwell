﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject _gameOverPanel;
    [SerializeField] private GameObject _pauseScreen;
    [SerializeField] private GameObject _player;
    [SerializeField] private GameObject _poisonedWater;
    [SerializeField] private WaterTrigger _waterTrigger;
    
    private CheckpointManager _checkPointManager;

    [Header("Score")]
    [SerializeField] private int _score;

    public int Score => _score;
    
    // Unity singleton pattern using properties
    private static GameManager _instance;
    public static GameManager Instance
    {
        get => _instance;
        set
        {
            if(_instance != null)
            {
                Destroy(value.gameObject);
                return;
            }
            _instance = value;
        }
    }

    private void Awake()
    {
        _checkPointManager = GameObject.FindGameObjectWithTag("CheckpointManager").GetComponent<CheckpointManager>();

        if (_checkPointManager.currentCheckPointId != 0)
        {
            //Debug.Log($"Player should respawn at checkpoint {_checkPointManager._currentCheckPointId}");
            _player.transform.position = _checkPointManager.currentCheckPointPosition;
            _score = _checkPointManager.scoreAtCurrentCheckpoint;

            if (_checkPointManager.IsTriggeringWater)
            {
                _poisonedWater.transform.position = _player.transform.position - new Vector3(0f, 30f, 0f); // put the water close to the player
                _poisonedWater.GetComponent<PoisonedWater>().IsWaterTriggered = true; //make the water come up;
                _waterTrigger.StartMusLoop();
            }
        }
    }

    private void Start()
    {
        // attempt to assign singleton
        Instance = this;

        _gameOverPanel.GetComponent<Animator>().SetTrigger("newGameTrigger");

    }


    // start gameOver logic
    public void GameOver()
    {
        StartCoroutine(GameOverRoutine());
    }

    private IEnumerator GameOverRoutine()
    {
        // wait
        yield return new WaitForSeconds(0.5f);

        // enable game over screen
        //_gameOverPanel.SetActive(true);
        _gameOverPanel.GetComponent<Animator>().SetTrigger("gameOverTrigger");

        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("RodrigoPlayGround");
    }

    // add to current score
    public void AddScore(int amount)
    {
        _score += amount;
    }
}
