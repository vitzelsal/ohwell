﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneControl : MonoBehaviour
{
    // loads the given scene
    public void LoadScene(string sceneName)
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(sceneName);
    }

    // quits the game
    public void QuitGame()
    {
        Application.Quit();
        Debug.LogWarning("Quitting!");
    }
}