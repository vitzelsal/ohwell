﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private float _fixedHeight = 2f;
    [SerializeField] private float _lerpSpeed = 5f;
    [SerializeField] private float _cameraOffsetXAxis = 0f;
    [SerializeField] private float _cameraOffsetYAxis = 9f;

    [SerializeField] private Transform _playerTransform;
    [SerializeField] private Transform _bossTransform;

    private CharacterController2D _playerController;
    private Vector3 _desiredPosition;

    private float _fixedDepth;

    public Transform Target {get {return _target;} set {_target = value;}}

    private void Awake()
    {
        _fixedDepth = transform.position.z;
        _playerController = _target.gameObject.GetComponent<CharacterController2D>();
    }

    private void Update()
    {
        _desiredPosition = new Vector3(_target.position.x + _cameraOffsetXAxis, _target.position.y + _cameraOffsetYAxis, _fixedDepth);
        this.transform.position = Vector3.Lerp(transform.position, _desiredPosition, _lerpSpeed * Time.deltaTime); 
    }

    public void SetPlayerAsTarget ()
    {
        _target = _playerTransform;
    }

    public void SetBossAsTarget ()
    {
        _target = _bossTransform;
    }
}
