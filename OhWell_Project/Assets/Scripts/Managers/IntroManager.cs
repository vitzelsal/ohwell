﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class IntroManager : MonoBehaviour
{
    [SerializeField]
    private VideoPlayer _videoPlayer;
    [SerializeField]
    private SceneControl _sceneControl;
    [SerializeField]
    private float _fadeTime = 1.5f;

    private Coroutine _fadeToRoutine = null;

    private float _timeIntroVideo;

    // Update is called once per frame
    IEnumerator Start()
    {
        yield return new WaitForSecondsRealtime((float)_videoPlayer.length +0.5f);
        //yield return FadeTo(0f);
        
        _sceneControl.LoadScene("Menu");
    }

    private WaitForSeconds FadeTo (float target)
    {
        if(_fadeToRoutine != null) StopCoroutine(_fadeToRoutine);
        _fadeToRoutine = StartCoroutine(FadeToRoutine(target));

        return new WaitForSeconds(_fadeTime);
    }

    IEnumerator FadeToRoutine (float target)
    {
        float origin = _videoPlayer.targetCameraAlpha;
        float step = 1 / _fadeTime;
        float progress = 0;

        while(progress < 0.995f)
        {
            _videoPlayer.targetCameraAlpha = Mathf.Lerp(origin, target, progress);
            progress += step * Time.deltaTime;
            yield return null;
        }

        _videoPlayer.targetCameraAlpha = target;
        _fadeToRoutine = null;
    }
}
