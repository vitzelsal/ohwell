﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Audio;

public class CreditsController : MonoBehaviour
{

    [SerializeField] 
    private GameObject _creditsScreen;
    [SerializeField] 
    private GameObject _alpahBackground;

	
	void Update()
	{
		if (Input.GetButtonDown(Commands._pauseInput)) 
		{
			_alpahBackground.SetActive(false);
            _creditsScreen.SetActive(false);
		}
	}
	
}
