﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;

    public AudioSource _audioSource;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
        }
        else 
        {
           Instance = this;
        }

        _audioSource = GetComponent<AudioSource>();
    }
}
