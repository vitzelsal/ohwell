﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using UnityEngine;

public class DebugManager : MonoBehaviour
{

    [SerializeField] private TMP_Dropdown dropdown;
    private int _optionFlag = -1;

    [SerializeField] private GameObject _playerObject;
    // key to press to spawn object
    [SerializeField] private KeyCode _spawnKey = KeyCode.Alpha1;

    // object to spawn
    [SerializeField] private GameObject _enemyPrefab;

    [SerializeField] private GameObject _collectablesPrefab;
    [SerializeField] private GameObject _mushroomPrefab;
    [SerializeField] private Canvas _debuggerInfo;
    [SerializeField] private TextMeshProUGUI _console;
    [SerializeField] private float _offset;


    private Vector3 _spawnedPosition;
    private Player _player;
    private CharacterController2D _playerCharacterController;


    // Start is called before the first frame update
    void Start()
    {
        dropdown.onValueChanged.AddListener(delegate {
            OptionChanged(dropdown);
        });
        _player = _playerObject.GetComponent<Player>();
        _playerCharacterController = _playerObject.GetComponent<CharacterController2D>();
    }

    // Update is called once per frame
    void Update()
    {
        _spawnedPosition = new Vector3 (_playerObject.transform.position.x+_offset, _playerObject.transform.position.y, _playerObject.transform.position.z);

        if(_optionFlag == 1)
        {
            DebugPlayer();
            //if(Input.GetKeyDown("Alpha2") _player.state = new DeadState(_player);
            
        }
        else if(_optionFlag >= 2)
        {
            _console.text = "Press 1 to spawn an object";
            if(Input.GetKeyDown(_spawnKey) && _optionFlag == 2)
            {     
                GameObject spawnedPreFab = Instantiate( _enemyPrefab, _spawnedPosition, _enemyPrefab.transform.rotation);  
            }
            else if(Input.GetKeyDown(_spawnKey) && _optionFlag == 3)
            {
                GameObject spawnedPreFab = Instantiate( _collectablesPrefab, _spawnedPosition, _collectablesPrefab.transform.rotation);  
            }
            else if(Input.GetKeyDown(_spawnKey) && _optionFlag == 4)
            {
                GameObject spawnedPreFab = Instantiate( _mushroomPrefab, _spawnedPosition, _mushroomPrefab.transform.rotation);  
            }
        }
        else
        {
            _console.text = "";
        }
        
    }

    public void OptionChanged(TMP_Dropdown option)
    {
       _optionFlag = option.value;

    }

    public void DebugPlayer()
    {
        var gameObjectInfo = "Position: " + _playerObject.transform.position.ToString() +
                             "\nVelocity: "+ _playerObject.GetComponent<Rigidbody2D>().velocity.ToString()+
                             "\nIs Grounded: "+ _playerCharacterController.IsGrounded.ToString() +
                             "\nIs Trying To Move: "+ _playerCharacterController.IsTryingToMove.ToString() +
                             "\nIsMovingRight: "+ _playerCharacterController.IsMovingRight.ToString() +
                             "\nKill player by pressing 2";


        _console.text = gameObjectInfo;
    }

}
