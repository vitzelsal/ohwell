﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CheckpointManager))]
public class Checkpoint : MonoBehaviour
{
    [SerializeField]
    private int _checkPointId;
    [SerializeField]
    private Transform _respawnPosition;
    [SerializeField]
    private bool _triggerWater;

    [Header("Sound Settings")]
    public GenericAudioPlayer audioPlayer;

    [Header("Sound Settings")]
    public AudioClip[] audioCheckpoint;
    
    private CheckpointManager _checkPointManager;
    private Animator _checkpointAnimator;

    public int Id {get{return _checkPointId;}}
    public Vector3 RespawnPosition {get{return _respawnPosition.position;}}
    public bool TriggerWater {get {return _triggerWater;}}

    private void Awake()
    {
        _checkPointManager = GameObject.FindGameObjectWithTag("CheckpointManager").GetComponent<CheckpointManager>();
    }

    private void Start()
    {
        _checkpointAnimator = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            audioPlayer.PlaySound(audioCheckpoint);
            _checkpointAnimator.enabled = true;
            _checkPointManager.AddNewCheckpoint(this);
        }
    }
}
