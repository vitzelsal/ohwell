﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Unit : MonoBehaviour
{
    protected CharacterController2D _characterController;
    protected Health _health;
    protected Animator _animator;

    [Header("Unit Sound Settings")]
    public GenericAudioPlayer audioPlayer;
    public AudioClip[] audioFootsteps;
    public AudioClip[] audioDead;

    public Health Health
    {
        get{ return _health;}
    }

    public CharacterController2D CharacterController
    {
        get { return _characterController;}
    }

    public Animator UnitAnimator
    {
        get { return _animator;}
    }

    private void Awake()
    {
        _characterController = GetComponent<CharacterController2D>();
        _health = GetComponent<Health>();
        _animator =  GetComponent<Animator>();

        // have Death() listen for DeathEvent
        // ? when DeathEvent is Invoked, Death() will execute
        _health.DeathEvent.AddListener(Death);
    }

    protected virtual void Death ()
    {

        this.gameObject.layer = LayerMask.NameToLayer("Corpse");
    }

    public AnimationClip GetAnimationClip (string name)
    {
        // no animator found
        if (_animator == null)
        {
            Debug.LogWarning($"The Game Object {this.gameObject.name} doesn't have an Animator");
            return null;
        }

        foreach(AnimationClip clip in _animator.runtimeAnimatorController.animationClips)
        {
            if (clip.name == name)
            {
                return clip;
            }
        }

        //No animation clip of that name found
        Debug.LogWarning($"No animation clip of the name {name} has been found");
        return null; 
    }   
}