﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [Header("Unit Sound Settings")]
    public GenericAudioPlayer audioPlayer;
    public AudioClip[] audioFireball;

    [SerializeField, Range(1f, 30f)]
    private float _projectileSpeed = 3f;
    private float _damage;

    private Health _health;

    public float Damage
    {
        get {return _damage;}
        set {_damage = value;}
    }

    public bool IsShootingRight{ get;set;}

    private void Awake()
    {
        _health = GetComponent<Health>();
    }

    void Update()
    {
       
        if (IsShootingRight)
            transform.Translate(transform.right * _projectileSpeed * Time.deltaTime);
        else
            transform.Translate(-transform.right * _projectileSpeed * Time.deltaTime);

        Destroy(gameObject, 7f); 
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // get possible health component
        Health health = other.gameObject.GetComponent<Health>();
        Player player = other.gameObject.GetComponent<Player>();
        //check if health exists
        if (health != null && player != null)
        {
            //check if health is enemy
            if (health.Team != this._health.Team)
            {
                //health.Damage(_damage);
                player.State = new TakingDamageState(player, 15f, 15f, _damage, false);
                Destroy(gameObject);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.layer != 8 && other.gameObject.layer != 10) // nor enemies or player
        {
            Destroy(gameObject);
        }
    }
}
