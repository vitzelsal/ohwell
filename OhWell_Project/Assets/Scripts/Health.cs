﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    [SerializeField] public float _current = 100f;
    [SerializeField] private float _max = 100f;
    [SerializeField] private int _team = 1;

    public bool IsAlive => _current > 0f; // ! if current is greater than 0, returns TRUE
    public int Team => _team; // ! same of doing {get; private set;}
    public float HealthPercentage => (_current/_max) *100; // ! returns current HP as %
    
    public UnityEvent DeathEvent;

    public void Damage (float amount)
    {
        // * only damage if alive
        if (IsAlive)
        {
            // * subtract damage
            _current -= amount;

            if (_current >= 100f)
                _current = 100f;
            else if (_current <=0)
                _current = 0f;
            
            //check for death
            if(!IsAlive)
            {
                // "Invoke" the event to execute any code listening
                DeathEvent.Invoke();
            }
        }
    }
}
