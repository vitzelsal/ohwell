﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimReticle : MonoBehaviour
{
    [SerializeField, Header("Player")]
    private Player _player;

    [SerializeField, Header("AimPosition")]
    private Transform _refPosition;
    [SerializeField]
    private Vector3 _aimPosOffset;

    private float _angle;
    private float _angleForRaycast;

    public float AngleForRaycast
    {
        get {return _angleForRaycast;}
    }

    private void Awake()
    {
        this.transform.position = _refPosition.position + _aimPosOffset;
    }

    private void Update()
    {
        float x = Input.GetAxisRaw(Commands._moveInput);
        float y = Input.GetAxisRaw(Commands._verticalInput);

        _angle = Mathf.Atan2(y, x) * Mathf.Rad2Deg;
        _angleForRaycast =_angle;

        //angle = Mathf.Clamp(angle, -30f, -150f);

        if (_angle > 90f && _angle <= 180f)
        {
            _angle = 180f - _angle;
            //Debug.Log($"Angle between 90 and 180: {angle}");
        }
        else if (_angle < -90 && _angle > -180f)
        {
            _angle = -180f - _angle;
            //Debug.Log($"Angle between -90 and -180: {angle}");
        }

        _angle = Mathf.Clamp(_angle, -45f, 90f);
        
        if (_player.CharacterController.IsMovingRight)
            this.transform.eulerAngles = new Vector3 (0f, 0f, _angle);
        else
            this.transform.eulerAngles = new Vector3 (0f, 0f, -_angle);
    }
}
