﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class Fly : MonoBehaviour
{
    [SerializeField, Range(1,2)]
    private int _heartsRecovered = 1;

    private float _healthAmountRecovered;
    private Health _health;

    [Header("Sound Settings")]
    public GenericAudioPlayer audioPlayer;

    [Header("Sound Settings")]
    public AudioClip[] audioIdle;

    private void Start()
    {
        _health = GetComponent<Health>();

        switch(_heartsRecovered)
        {
            case 1:
                _healthAmountRecovered = 34f;
                break;
            case 2:
                _healthAmountRecovered = 67f;
                break;
            default:
                _healthAmountRecovered = 34f;
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        audioPlayer.PlaySound(audioIdle);
        // get possible health component
        Health health = other.gameObject.GetComponent<Health>();
        //check if health exists
        if (health != null)
        {
            //check if health is enemy
            if (health.Team == this._health.Team)
            {
                health.Damage(-_healthAmountRecovered);
                Destroy(gameObject);
            }
        } 
    }

}
