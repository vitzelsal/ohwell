﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour
{

    private void OnCollisionEnter2D(Collision2D other)
    {
        // get possible player component
        Player player = other.gameObject.GetComponent<Player>();

        //check if player exists
        if (player != null)
        {
            if (player.Health.IsAlive)
                player.State = new TakingDamageState(player, 0f, 30f, 34f, true);
        }                
    }
}
