﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Enemy : Unit
{
    [Header ("Enemy Sound Clips")]
    public AudioClip[] audioIdle;

    [SerializeField, Header("Parameters")]
    protected float _speed = 5f;
    [SerializeField]
    private Transform _pointToGrab;

    [SerializeField, Header ("Patrol Behaviour")]
    protected bool _willPatrol;
    [SerializeField, Range(1f,20f)]
    protected float _patrolLenght = 15f;
    [SerializeField]
    protected bool _isStartPositionFacingRight;

    protected Vector3 _originalPos; 
    protected Vector3 _patrolTarget;
    private float _speedMultiplier;

    [SerializeField, Header("Shoot Projectiles Behaviour")]
    protected bool _willShootProjectiles;
    [SerializeField]
    protected  GameObject _projectilePrefab;
    [SerializeField, Range(1,10)]
    protected int _secondsBetweenShots = 3;
    [SerializeField, Range (1,3)]
    protected int _damageTakenInHearts = 1;
    [SerializeField]
    protected Transform _projectileBarrel;

    protected float _projectileDamage;
    protected float _projectileTimer;

    private SpriteRenderer[] _enemySpriteRenderer;
    private Rigidbody2D _enemyRB;
    [HideInInspector]
    public bool isTargeted;

    public Transform PointToGrab {get {return _pointToGrab;}}
    
    
    private void Start()
    {
        _enemySpriteRenderer = GetComponentsInChildren<SpriteRenderer>();
        _enemyRB = GetComponent<Rigidbody2D>();

        if (_willPatrol)
        {   
            _speedMultiplier = _isStartPositionFacingRight ? 1.0f:-1.0f;
            _originalPos =  _isStartPositionFacingRight ? new Vector3(transform.position.x - 0.2f, transform.position.y, transform.position.z) : new Vector3(transform.position.x + 0.2f, transform.position.y, transform.position.z);
            _patrolTarget = _isStartPositionFacingRight ? new Vector3(transform.position.x + _patrolLenght, transform.position.y, transform.position.z) : new Vector3(transform.position.x - _patrolLenght, transform.position.y, transform.position.z);
        }
        if(_willShootProjectiles)
        {
            switch(_damageTakenInHearts)
            {
                case 1:
                    _projectileDamage = 34f;
                    break;
                case 2:
                    _projectileDamage = 67f;
                    break;
                case 3:
                    _projectileDamage = 100f;
                    break;
                default:
                    break;
            }
            _projectileTimer = Time.time;
        }
    }

    private void Update()
    {   
        _animator.SetFloat("speed", Mathf.Abs(_enemyRB.velocity.x));

        //Activating the enemy animator only if he is visible by the camera 
        if (_enemySpriteRenderer[0].isVisible)
            UnitAnimator.enabled = true;
        else
            UnitAnimator.enabled = false;   
        //*****************************************************************

        //Freezing the enemy in a possible Targeted condition
        if (isTargeted)
        {
            _enemyRB.velocity = Vector2.zero;
        }

        #region  PatrolBehaviour
        //PatrolBehaviour
        if (_willPatrol && !isTargeted && Health.IsAlive)
        {
            transform.localScale = new Vector3(-Mathf.Sign(_speedMultiplier), 1, 1); //changing the direction enemy is facing according to the speed multiplier

            _enemyRB.velocity = new Vector2(_speed * _speedMultiplier, 0f);
            //_enemyRB.velocity = transform.right * -_speed * _speedMultiplier;

            if (_isStartPositionFacingRight)
            {
                 if (transform.position.x > _patrolTarget.x)
                {
                    _speedMultiplier = -1 * Mathf.Abs(_speedMultiplier); //ALWAYS GOING LEFT
                    //transform.rotation = Quaternion.Euler(0f, 180f,0f);
                }
                else if(transform.position.x < _originalPos.x)
                {
                    _speedMultiplier = 1 * Mathf.Abs(_speedMultiplier); //ALWAYS GOING RIGHT
                    //transform.rotation = Quaternion.Euler(0f, 0f,0f);
                }
            }
            else
            {
                if (transform.position.x < _patrolTarget.x)
                {
                    _speedMultiplier = 1 * Mathf.Abs(_speedMultiplier); //ALWAYS GOING RIGHT
                    //transform.rotation = Quaternion.Euler(0f, 180f,0f);
                }
                else if(transform.position.x > _originalPos.x)
                {
                    _speedMultiplier = -1 * Mathf.Abs(_speedMultiplier); //ALWAYS GOING LEFT
                    //transform.rotation = Quaternion.Euler(0f, 0f,0f);
                }
            }
        }
        #endregion

        #region ShootProjectilesBehaviour
        //Shoot Projectiles behaviour
        if (_willShootProjectiles && !isTargeted && Health.IsAlive)
        {
            if (Time.time - _projectileTimer >= _secondsBetweenShots)
            {
                if (_projectilePrefab != null)
                {
                    var projectileObject = Instantiate(_projectilePrefab, _projectileBarrel.position, _projectileBarrel.rotation);
                    
                    if (this.transform.localScale.x == -1)
                        projectileObject.GetComponent<Projectile>().IsShootingRight = true;
                    else
                        projectileObject.GetComponent<Projectile>().IsShootingRight = false;

                    projectileObject.GetComponent<Projectile>().Damage = _projectileDamage;

                }
                _projectileTimer = Time.time;
            }
        }
        #endregion
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        // get possible health component
        Health health = other.gameObject.GetComponent<Health>();

        //check if health exists
        if (health != null)
        {
            //check if health is enemy
            if (health.Team != this.Health.Team)
            {
                var player = other.gameObject.GetComponent<Player>();

                if (player != null && (Random.value <= AnimationAudioEvent._voiceProbability)) player.audioPlayer.PlaySound(player.voHurt);
                
                player.State = new TakingDamageState(player, 15f, 15f, 34f, false);
            }
        } 
        //Unfreezing the enemy after a Targeted condition
        isTargeted = false;               
    }

    //TODO TRIGGERING THE ATTACK
    private void OnTriggerEnter2D(Collider2D other)
    {
        // get possible health component
        Health health = other.gameObject.GetComponent<Health>();
        //check if health exists
        if (health != null)
        {
            //check if health is enemy
            if (health.Team != this.Health.Team)
            {
                Debug.Log("I should attack");
                // isTargeted = true; //use this boolean to freeze the enemy to attack
                // _animator.SetTrigger("attackTrigger");
            }
        } 
    }

    protected override void Death()
    {
        base.Death();
        this.audioPlayer.PlaySound(this.audioDead);
        if (_animator != null)
        {
            _animator.SetTrigger("deathTrigger");
        }
        Destroy(this.gameObject, 3f);
    }
}
