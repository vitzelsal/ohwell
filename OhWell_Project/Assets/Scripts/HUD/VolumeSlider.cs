﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeSlider : MonoBehaviour
{
    
    [SerializeField]
    private Slider _musicSlider;

    [SerializeField]
    private GameObject _environmentAudioObj;
    private AudioSource _environmentAudio;

    private void Awake()
    {
        _musicSlider = this.gameObject.GetComponent<Slider>();
    }

    private void Start()
    {
        _environmentAudio = _environmentAudioObj.GetComponent<AudioSource>();
    }

    private void Update()
    {
        _environmentAudio.volume = _musicSlider.value;
    }
}
