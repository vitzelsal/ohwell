﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreDisplay : MonoBehaviour
{
    // strings added to the start and end of score
    [SerializeField] private string _prefix = "X ";
    [SerializeField] private string _suffix = "";

    private TextMeshProUGUI _textMesh;

    private void Start()
    {
        _textMesh = GetComponent<TextMeshProUGUI>();
    }

    private void Update()
    {
        // update score text
        _textMesh.text = _prefix + GameManager.Instance.Score + _suffix;
    }
}
