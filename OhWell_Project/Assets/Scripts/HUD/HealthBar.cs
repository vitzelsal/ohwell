﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HealthBar : MonoBehaviour
{
    // the health component we're displaying
    [SerializeField] private Health _target;
    [SerializeField] private Sprite _emptyHeart;
    [SerializeField] private Sprite _fullHeart;
    [SerializeField] private float _minHealth;

    private Image _image;

    private void Start()
    {
        _image = GetComponent<Image>();
    }

    private void Update()
    {
        // set to 0 if target is null
        if(_target == null)
        {
            _image.sprite = _emptyHeart;
            
            return;
        }
        else if(_target.HealthPercentage < _minHealth)
        {
            _image.sprite = _emptyHeart;
        }
        else if (_target.HealthPercentage >= _minHealth)
        {
            _image.sprite = _fullHeart;
        }
        
    }
}
