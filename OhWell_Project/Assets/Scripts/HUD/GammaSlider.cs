﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;

public class GammaSlider : MonoBehaviour
{
    [SerializeField]
    private Slider _gammaSlider;


    private PostProcessProfile _postProcessingProfile;
    private float _gammaValue = 1f;
    
    public float GammaValue {get{ return _gammaValue;} set { _gammaValue = value;}}

    private void Awake()
    {
        _postProcessingProfile = GetComponent<PostProcessVolume>().profile;
    }

    private void Update()
    {
        _gammaValue = _gammaSlider.value;
        _postProcessingProfile.GetSetting<ColorGrading>().toneCurveGamma.value = _gammaValue;
    }
}
